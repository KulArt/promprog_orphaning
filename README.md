# promprog_orphaning

Here is a sequence of commands:

```
git checkout --orphan codereview
git merge master
git checkout master
git reset --hard HEAD~1 # Go back 1 commit.
git checkout codereview
```

[Link](https://stackoverflow.com/questions/1628563/move-the-most-recent-commits-to-a-new-branch-with-git) to solution

